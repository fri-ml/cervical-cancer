# Task:

Features: All the columns except the last one (X)

To predict: The last column is the label (Biopsy) (Y)So, we need to map X -> Y (find Y, given X)Steps:The following are the standard steps in developing a ML model. However, fastai takes care of a lot of things and we need to understand what is going on at each stage.

## Data Exploration and analysis

    * Clean data
        Remove any missing values, nans, infs or values that do not make sense
    * Take a look at the data and understand
        The distribution of each of the feature, do they make sense?
        Is the data balanced? (for cancer the data is imbalanced, i.e., a lot more negative values (no cancer) than positive values (cancer). These type of problems require slightly different methods
    
    * Preprocess data

        + Scaling
        + Transform if you think it is necessary (log transform or other advanced transformations of certain features)Split data into test and train sets

## Split the data into test set and train set and just work with the train set from now on
    Be careful as we have an imbalanced dataset … we should do up-sampling so both positive and negative data gets represented in the train and test setBuild a model
    Build a neural network
    Choose different parameters (# of layers, nodes in each layer and so on )
    
## Train the model

    Train using various options -- -learning rates, cyclic learning, learning with restarts, different batch sizes, algorithms and so on
    Use cross-validation
    
## Test the model

    Test on the final test set
